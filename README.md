Raspberry Pi: Ubuntu tools
====================

This repository contains scripts I use for my Raspberry Pi 4 Model B running Ubuntu, however most stuff probably also work on other boards and other debian based distros like Kali. 
Must be run as root or with sudo.

```bash
ubuntu@ubuntu:~$ git clone https://gitlab.com/nickfreeman/raspi_ubuntu_tools.git
Cloning into 'raspi_ubuntu_tools'...
[...]
ubuntu@ubuntu:~$ cd raspi_ubuntu_tools/
```

Stuff you can then do:
- `sudo ./install_raspi_config.sh` to install [raspi-config](https://www.raspberrypi.org/documentation/configuration/raspi-config.md).
- `sudo ./connect_wireless.sh` to connect to a (simple) wifi network.
