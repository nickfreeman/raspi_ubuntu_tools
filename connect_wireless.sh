#!/bin/sh
# Check if root
if [ "$(whoami)" != "root" ]; then
  whiptail --msgbox "Sorry you are not root.\nYou must type: \"sudo ./connect_wireless.sh\"" 10 60
  exit
fi

SSID=$(whiptail --inputbox "ssid (name of wifi):" 8 78 3>&1 1>&2 2>&3)
PASSWORD=$(whiptail --inputbox "password:" 8 78 3>&1 1>&2 2>&3)

if (whiptail --yesno "Do you want to connect to\n\"$SSID\"\nwith password\n\"$PASSWORD\"\n?" 10 60); then
  echo "yes"
  sudo cat > /etc/netplan/wireless.yaml <<- EOM
network:
  version: 2
  renderer: networkd
  wifis:
    wlan0:
      dhcp4: yes
      dhcp6: yes
      access-points:
        "$SSID":
          password: "$PASSWORD"
EOM
  echo "applying changes ..."
  sudo netplan try
  echo "File: /etc/netplan/wireless.yaml"
  echo "Reload: 'sudo netplan try'"
  exit 0
else
  exit 1
fi
