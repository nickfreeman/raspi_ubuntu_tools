#!/bin/sh
# install dependencies
apt-get install libnewt0.52 whiptail parted triggerhappy lua5.1 alsa-utils -y
# Check if root
if [ "$(whoami)" != "root" ]; then
  whiptail --msgbox "Sorry you are not root.\nYou must type: \"sudo ./install_raspi_config.sh.sh\"" 10 60
  exit
fi

# Check if raspi-config is installed
if [ $(dpkg-query -W -f='${Status}' raspi-config 2>/dev/null | grep -c "ok installed") -eq 1 ]; then
  if (whiptail --yesno "Raspi-config is already installed. Do you want to reinstall/update it?" 10 60); then
    sudo apt remove raspi-config -y
  else
    exit 1
  fi
fi

wget https://archive.raspberrypi.org/debian/pool/main/r/raspi-config/raspi-config_20210212_all.deb -O /tmp/raspi-config.deb
# Auto install dependencies on eg. ubuntu server on RPI
apt-get install -fy
dpkg -i /tmp/raspi-config.deb
rm /tmp/raspi-config.deb
whiptail --msgbox "Raspi-config is now installed, run it by typing: sudo raspi-config" 10 60

exit 0
