#!/bin/sh
for i in /sys/class/leds/led*
do
whiptail --title "Example Dialog" --yesno "Do you want to disable the led\n$i" 8 78
echo "$i set to $(echo $? | sudo tee $i/brightness)"
done
